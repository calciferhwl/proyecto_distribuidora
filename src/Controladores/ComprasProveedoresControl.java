/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Clases.DetallePedido;
import Clases.PedidoProveedores;
import Clases.Producto;
import Clases.Proveedor;
import DAOS.DetallePedidoProveedorJpaController;
import DAOS.PedidoProveedoresJpaController;
import DAOS.ProductoJpaController;
import DAOS.ProveedorJpaController;
import java.util.LinkedList;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author sebastian
 */
public class ComprasProveedoresControl {
    
    
    PedidoProveedoresJpaController pedidosProveedores;
    ProveedorJpaController proveedores;
    ProductoJpaController productos;

    public ComprasProveedoresControl() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Proyecto_distribuidorasPU");
        
        pedidosProveedores = new PedidoProveedoresJpaController(emf);
        proveedores = new ProveedorJpaController(emf);
        productos = new ProductoJpaController(emf);
    }
    
    
    public LinkedList<Producto> buscarProductosPorAgotarse(int cantidadMinima){
        //Scalar function
        return productos.getProductosAgotarse(cantidadMinima);
    }

    
    public Proveedor buscarProveedor(int codigo){
        return proveedores.findProveedor(codigo);
    }
    
    public void crearProveedor(Proveedor prove) throws Exception{
        proveedores.create(prove);
    }
    
    public LinkedList<Proveedor> buscarProveedoresCodigoProducto(int codigo_producto){
        return proveedores.getProveedoresDitribucion(codigo_producto);
    }  

    public PedidoProveedores buscarPedidoPorProveedor (Proveedor prove1) throws Exception{
       
       return this.pedidosProveedores.buscarPedidoPorProveedor(prove1.getCodigoProveedor());
   }

    public void guardarPedido(PedidoProveedores pedido){
        pedidosProveedores.create(pedido);
    }
    
    
    public Producto buscarProducto(int codigo) throws Exception{
        return productos.buscarProducto(codigo);
    }
}
