/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Clases.Cliente;
import DAOS.ClienteJpaController;
import DAOS.PedidoClientesJpaController;
import DAOS.ProductoJpaController;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author sebastian
 */
public class ControlPedidoCliente {
    
    ClienteJpaController clientes;
    ProductoJpaController productos;
    PedidoClientesJpaController pedidosCliente;

    public ControlPedidoCliente() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Proyecto_distribuidorasPU");
        clientes = new ClienteJpaController(emf);
        productos = new ProductoJpaController(emf);
        pedidosCliente = new PedidoClientesJpaController(emf);
    }

    public ControlPedidoCliente(ClienteJpaController clientes, ProductoJpaController productos, PedidoClientesJpaController pedidosCliente) {
        this.clientes = clientes;
        this.productos = productos;
        this.pedidosCliente = pedidosCliente;
    }
    
    public Cliente buscarCliente(long nit) throws Exception{
        return clientes.findCliente(nit);
    }
    
    
    
}
