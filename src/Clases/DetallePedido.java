/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import java.util.List;

/**
 *
 * @author sebastian
 */

@MappedSuperclass
@Inheritance (strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class DetallePedido implements Serializable{
    
    @Id
    @Column(nullable = false,length = 20)
    private int codigo_pedido;
     
    private LocalDate fechaCreacion;
    private LocalDate fechaDespacho;
    private LocalDate fecahLlegada;
  
  //uno a muchos con producto//////////////////////////////////////////////////
  @OneToMany
  private List<Producto>listaproductos;
            
    //constructor///////////////////////////////////////////////////////////////

    public DetallePedido() {
    }

    public DetallePedido(int codigo_pedido, LocalDate fechaCreacion, LocalDate fechaDespacho, LocalDate fecahLlegada)throws Exception {
        if(codigo_pedido<0){
        throw new Exception("EL CODIG DEL PEDIDO DEBE SER MAYOR A 0");
    }
        this.codigo_pedido = codigo_pedido;
        this.fechaCreacion = fechaCreacion;
        this.fechaDespacho = fechaDespacho;
        this.fecahLlegada = fecahLlegada;
        this.listaproductos = new LinkedList<>();
    }

 //get//////////////////////////////////////////////////////////////////////////

    public int getCodigo_pedido() {
        return codigo_pedido;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public LocalDate getFechaDespacho() {
        return fechaDespacho;
    }

    public LocalDate getFecahLlegada() {
        return fecahLlegada;
    }
    
    //set///////////////////////////////////////////////////////////////////////

    public void setCodigo_pedido(int codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setFechaDespacho(LocalDate fechaDespacho) {
        this.fechaDespacho = fechaDespacho;
    }

    public void setFecahLlegada(LocalDate fecahLlegada) {
        this.fecahLlegada = fecahLlegada;
    }
    
    //////////////////////////////////////////////////////////////////////////

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetallePedido other = (DetallePedido) obj;
        if (this.codigo_pedido != other.codigo_pedido) {
            return false;
        }
        if (!Objects.equals(this.fechaCreacion, other.fechaCreacion)) {
            return false;
        }
        if (!Objects.equals(this.fechaDespacho, other.fechaDespacho)) {
            return false;
        }
        if (!Objects.equals(this.fecahLlegada, other.fecahLlegada)) {
            return false;
        }
        return true;
    }
    
    
}
