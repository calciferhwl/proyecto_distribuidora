/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Clases;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


     

/**
 *
 * @author sebastian
 */
@Entity
public class Cliente implements Serializable{
    
    @Id
    @Column(nullable = false)
    private long nit;
    private String nombre;
    private String direccion;
    private long telefono;
    
    //relacion con pedido clientes 
    @OneToMany
    private List<PedidoClientes> pedidosCliente;
    

    
    //Constructor
      
    public Cliente() {
        pedidosCliente = new LinkedList<>();
    }
    
    public Cliente(long Nit, String nombre, String direccion,long telefono) throws Exception//throws Exception/
    {
//        if(Nit<0){
//            throw new Exception("EL NIT DEBE SER MAYOR A 0");
//        }
        if(nombre == null || "".equals(nombre.trim())){
            throw new Exception("POR FAVOR NO DEJE EL NOMBRE VACIO");
        }
        if (direccion == null || "".equals(direccion.trim())){
          throw new Exception("POR FAVOR NO DEJE LA DIRECCION VACIA");
        }
        if (telefono<0){
          throw new Exception("POR FAVOR NO DEJE EL TELEFONO VACIO");
        }
        this.nit = Nit;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono=telefono;
        this.pedidosCliente = new LinkedList<>();
    }

    public long getNit() {
        return nit;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public long getTelefono() {
        return telefono;
    }

    public void setNit(long Nit) {
        this.nit = Nit;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + (int) (this.nit ^ (this.nit >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (this.nit != other.nit) {
            return false;
        }
        return true;
    }

    

    
    
    
}




    
    
    
