/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 *
 * @author sebastian
 */
@Entity
public class Encargado implements Serializable {
        @Id
        @Column(nullable = false, length = 15)
        private long identificacion;
        
        @Column(nullable = false, length = 40)
        private String nombre;
            
        
        @Column(nullable = false, length = 40)
        private String apellido;
        
        @Column(nullable = false,length = 30)
        private long telefono;
        
        //uno a uno con pedido clientes///////////////////////////////////
        @OneToOne
        private PedidoClientes pedidoCliente;
        
        //uno a uno con pedido proveedor/////////////////////////////////
        @OneToOne
        private PedidoProveedores pedidoProveedor;
        
        //uno a uno con rol////////////////////////////////////////////////
        @JoinColumn(name = "fk_rol")
        @OneToOne
        private Rol rol;
        
        
// constructor//////////////////////////////////////////////////////////
    public Encargado() {
    }

    public Encargado(long identificacion, String nombre, String apellido, long telefono, PedidoClientes perClientes, PedidoProveedores pedidoProveedores, Rol rol) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.pedidoCliente = perClientes;
        this.rol = rol;
    }
 
    public Encargado(long identificacion, String nombre, String apellido, long telefono)throws Exception {
        if (identificacion<0){
            throw new Exception("LA IDENTIFICACION DEBE SER MAYOR A 0");
        }
        if(nombre== null||"".equals(nombre.trim())){
            throw new Exception("POR FAVOR NO DEJE EL NOMBRE VACIO");
        }
         if(apellido== null||"".equals(apellido.trim())){
            throw new Exception("POR FAVOR NO DEJE EL APELLIDO VACIO");
        }
         if(telefono<0){
            throw new Exception("POR FAVOR NO DEJE EL TELEFONO VACIO");
        }
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
    }
//get///////////////////////////////////////////////////////////////
    public long getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public long getTelefono() {
        return telefono;
    }
// set/////////////////////////////////////////////////////////////////////
    public void setIdentificacion(long identificacion) {
        this.identificacion = identificacion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setTelefono(long telefono) {
        this.telefono = telefono;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Encargado other = (Encargado) obj;
        if (this.identificacion != other.identificacion) {
            return false;
        }
        if (this.telefono != other.telefono) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.apellido, other.apellido)) {
            return false;
        }
        return true;
    }
        
        
        
    
}
