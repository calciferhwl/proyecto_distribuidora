/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author sebastian
 */
@Entity
//@NamedQueries({
//    @NamedQuery(name = "buscarPorProveedor", query = "SELECT p FROM PEDIDOPROVEEDORES p WHERE p.codigo_proveedor = :codigo")})
public class PedidoProveedores implements Serializable{
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long pk;
    
    private LocalDate fechaCreacion;
    private String estado; // 
    private int codigo_proveedor;
    //uno amuchos con proveedor //////////////////////////////////////////////////
    @JoinColumn(name = "fk_proveedor")
    @ManyToOne
    private Proveedor proveedor;
    // uno a uno con encargado///////////////////////////////////////////////////
    @OneToOne
    private Encargado encargado;
//constructor/////////////////////////////////////////////////////////////////////
    @OneToMany
    private List<DetallePedidoProveedor> Detallesproductos;
    
    @OneToMany
    private List<Producto> productos;
    
    public PedidoProveedores() {
        this.productos = new LinkedList<>();
    }

    public PedidoProveedores(Proveedor proveedor, Encargado encargado) {
        this.proveedor = proveedor;
        this.encargado = encargado;
        this.productos = new LinkedList<Producto>();
    }

    public PedidoProveedores(Proveedor proveedor, Encargado encargado, int codigo_pedido, LocalDate fechaCreacion, LocalDate fechaDespacho, LocalDate fecahLlegada) throws Exception {
       
        this.proveedor = proveedor;
        this.encargado = encargado;
        this.productos = new LinkedList<>();
    }
    public Proveedor getProveedor() {
        return proveedor;
    }

    public int getCodigo_proveedor() {
        return codigo_proveedor;
    }

    public void setCodigo_proveedor(int codigo_proveedor) {
        this.codigo_proveedor = codigo_proveedor;
    }
    
    public Encargado getEncargado() {
        return encargado;
    }

    public void setProveedor(Proveedor proveedor) {
        this.setCodigo_proveedor(proveedor.getCodigoProveedor());
        this.proveedor = proveedor;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<DetallePedidoProveedor> getDetallesproductos() {
        return Detallesproductos;
    }

    public void setDetallesproductos(List<DetallePedidoProveedor> Detallesproductos) {
        this.Detallesproductos = Detallesproductos;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.proveedor);
        hash = 67 * hash + Objects.hashCode(this.encargado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PedidoProveedores other = (PedidoProveedores) obj;
        if (!Objects.equals(this.proveedor, other.proveedor)) {
            return false;
        }
        if (!Objects.equals(this.encargado, other.encargado)) {
            return false;
        }
        return true;
    }

    public long getPk() {
        return pk;
    }

    
   public void añadirProducto(Producto producto){
       this.productos.add(producto);
   }
    
    
            

   
    
    
}
