/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import Clases.Inventario;
/**
 *
 * @author sebastian
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "buscarPorCodigo", query = "SELECT p FROM Producto p WHERE p.codigo_producto = :codigo")})
public class Producto implements Serializable {
    @Id
    private int codigo_producto;
    private String nombre;
    private int cantidad;
    private float valorUnidad;
    private float costoUnidad;
    
    @OneToOne
    private Proveedor proveedor;
    
    //uno a muchos con detalle producto/////////////////////////////////////
    @OneToMany
    private List<DetalleProducto> detalleProductos;
    
    @OneToOne
    private PedidoProveedores pedidoProveedor;
    @OneToOne
    private PedidoClientes pedidoClientes;
    
//constructor///////////////////////////////////////////////////////////////////
    public Producto() {
    }

    public Producto(int codigo_producto, String nombre, int cantidad , float valor , float costo)throws Exception {
        if(codigo_producto<0){
            throw  new Exception("INGRESE UN CODIGO VALIDO");
        }
        this.codigo_producto = codigo_producto;
        this.nombre = nombre;
        if(cantidad<0){
            throw  new Exception("INGRESE UN CODIGO VALIDO");
        }
        this.valorUnidad = valor;
        this.cantidad = cantidad;
        this.detalleProductos = new LinkedList();
    }

    
        
    
//get///////////////////////////////////////////////////////////////////////////
    public int getCodigo_producto() {
        return codigo_producto;
    }

    public String getNombre() {
        return nombre;
    }

    public float getValorUnidad() {
        return valorUnidad;
    }

    public void setValorUnidad(float valorUnidad) {
        this.valorUnidad = valorUnidad;
    }

    public float getCostoUnidad() {
        return costoUnidad;
    }

    public void setCostoUnidad(float costoUnidad) {
        this.costoUnidad = costoUnidad;
    }

    public PedidoProveedores getPedidoProveedor() {
        return pedidoProveedor;
    }

    public void setPedidoProveedor(PedidoProveedores pedidoProveedor) {
        this.pedidoProveedor = pedidoProveedor;
    }

    public PedidoClientes getPedidoClientes() {
        return pedidoClientes;
    }

    public void setPedidoClientes(PedidoClientes pedidoClientes) {
        this.pedidoClientes = pedidoClientes;
    }

    

   

    public Proveedor getProveedor() {
        return proveedor;
    }

    public int getCantidad() {
        return cantidad;
    }

    public float getValor() {
        return valorUnidad;
    }

    public void setValor(float valor) {
        this.valorUnidad = valor;
    }


    public void setCosto(float costo) {
        this.costoUnidad = costo;
    }

    
     

    
    // set//////////////////////////////////////////////////////////////////////

    public void setCodigo_producto(int codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public void setDetalleProductos(List<DetalleProducto> detalleProductos) {
        this.detalleProductos = detalleProductos;
    }

    

   

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
//

    public List<DetalleProducto> getDetalleProductos() {
        return detalleProductos;
    }

    public void setDetalleProductos(LinkedList<DetalleProducto> detalleProductos) {
        this.detalleProductos = detalleProductos;
    }

    public void setDetalleProductos(ArrayList<DetalleProducto> attachedDetalleProductos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
    
}
