/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author sebastian
 */
@Entity
public class Rol implements Serializable{
    @Id
        private int codigo;
    @Column(nullable = false)
        private EnumRol rol;
    //uno a uno con encargado////////////////////////////////////////////////
        @OneToOne
        private Encargado encargado;
        
   //constructor//////////////////////////////////////////////////////////////
    
    public Rol() {
    }

    public Rol(int codigo, EnumRol rol, Encargado encargado) {
        this.codigo = codigo;
        this.rol = rol;
        this.encargado = encargado;
    }
   

    public EnumRol getRol() {
        return rol;
    }

    public Encargado getEncargado() {
        return encargado;
    }

    public void setRol(EnumRol rol) {
        this.rol = rol;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Rol other = (Rol) obj;
        if (this.rol != other.rol) {
            return false;
        }
        if (!Objects.equals(this.encargado, other.encargado)) {
            return false;
        }
        return true;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
        
                
}
