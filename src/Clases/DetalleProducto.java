/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author sebastian
 */
@Entity
public class DetalleProducto implements Serializable{
    
    
    @Column(nullable = false)
    private int cantidad;
    private int cantidadSalida;
    private int cantidadEntrada;
    //relacion con producto/////////////////////////////////////////////////////
    @JoinColumn(name = "fk_producto")
    @OneToOne
    private Producto producto;    
//construuctor//////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    

    public DetalleProducto() {
    }

    public DetalleProducto(int cantidad, int cantidadEntrada,int cantidadSalida, Producto producto)throws Exception {
       
        if(cantidad <= 0){
            throw new Exception("POR FAVOR INGRESE UNA CANTIDAD MAYOR A 0");
           
        }
        this.cantidad = cantidad;
        this.producto = producto;
        this.cantidadEntrada = cantidadEntrada;
        this.cantidadSalida = cantidadSalida;
    }
   
    
   
//get///////////////////////////////////////////////////////////////////////////
   
    public int getCantidad() {
        return cantidad;
    }

    

    public Producto getProducto() {
        return producto;
    }

    
    
//set///////////////////////////////////////////////////////////////////////////

    public int getCantidadSalida() {
        return cantidadSalida;
    }

    public void setCantidadSalida(int cantidadSalida) {
        this.cantidadSalida = cantidadSalida;
    }

    public int getCantidadEntrada() {
        return cantidadEntrada;
    }

    public void setCantidadEntrada(int cantidadEntrada) {
        this.cantidadEntrada = cantidadEntrada;
    }

    
    

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }


    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
////////////////////////////////////////////////////////////////////////////////

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleProducto other = (DetalleProducto) obj;
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (!Objects.equals(this.producto, other.producto)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


} 