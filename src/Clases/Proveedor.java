/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author sebastian
 */
@Entity
public class Proveedor implements Serializable{
    @Id
    @Column(nullable = false,length = 20)    
    private int codigoProveedor;
    
    @Column(nullable = false,length = 20)
    private int nit;
    
    @Column(nullable = false,length = 40)
    private String nombre;
    
    @Column(nullable = false,length = 40)
    private String direccion;
    
    //uno a muchos con productos /////////////////////////////////////////////
    @OneToMany
    private List<Producto> listaproductos;
    
    //uno amuchos con pedido proveedor///////////////////////////////////////
//    @OneToMany
//    private ArrayList<PedidoProveedores>pedidosproveedor = new ArrayList<>();
///constructor//////////////////////////////////////////////////////////////////
    public Proveedor() {
    }

    public Proveedor(int codigoProveedor, int nit, String nombre, String direccion)throws Exception {
        if(codigoProveedor<0){
            throw new Exception("PRO FAVOR INGRESE EL CODIGO DE PROVEEDOR");
        }
        if(nit<0){
            throw new Exception("PRO FAVOR INGRESE EL NIT DE PROVEEDOR");
        } 
        if(nombre == null || "".equals(nombre.trim())){
            throw new Exception("POR FAVOR INGRESE EL NOMBRE");
        }
        if (direccion == null || "".equals(direccion.trim())){
          throw new Exception("POR FAVOR INGRESE LA DIRECCION");
        }
        
        this.codigoProveedor = codigoProveedor;
        this.nit = nit;
        this.nombre = nombre;
        this.direccion = direccion;
        this.listaproductos = new LinkedList<>();
    }
//get///////////////////////////////////////////////////////////////////////////
    public int getCodigoProveedor() {
        return codigoProveedor;
    }

    public int getNit() {
        return nit;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }
//set///////////////////////////////////////////////////////////////////////////
    public void setCodigoProveedor(int codigoProveedor) {
        this.codigoProveedor = codigoProveedor;
    }

    public void setNit(int nit) {
        this.nit = nit;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public List<Producto> getListaProductos() {
        return listaproductos;
    }

    public void setListaproductos(List<Producto> listaproductos) {
        this.listaproductos = listaproductos;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Proveedor other = (Proveedor) obj;
        if (this.codigoProveedor != other.codigoProveedor) {
            return false;
        }
        if (this.nit != other.nit) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        return true;
    }
     
    
    public void añadirProducto(Producto producto){
        this.listaproductos.add(producto);
    }

    public boolean ditribuyeProducto (int codigoProducto){
        for(Producto p : listaproductos){
            if(p.getCodigo_producto() == codigoProducto){
                return true;
            }
        }
        return false;
    }
    
    public Producto buscarProducto(int codigo) throws Exception{
        for(Producto p : listaproductos){
            if(p.getCodigo_producto() == codigo){
                return p;
            }
        }
        throw new Exception("El producto no se encontró");
    }
    
    public void salidaProducto(){
        
    }
}
