/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author sebastian
 */
@Entity
public class PedidoClientes implements Serializable{
    
    
    //una a muchos con cliente//////////////////////////////////////////
    @JoinColumn(name = "fk_cliente", nullable = false)
    @ManyToOne
    private Cliente cliente;
    
    // uno a uno con encargado//////////////////////////////////////////
    @JoinColumn(name = "fk encargado", nullable = false)
    @OneToOne
    private Encargado encargado;
    ///////////////////////////////////////////////////////////////////
//    @OneToMany
//    private List<DetallePedidoCliente> detallesPedidos;
      @OneToMany
      private List<Producto> productos;
      
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pk;
    
    
    public PedidoClientes() {
    }
    
    
    public PedidoClientes(Cliente cliente, Encargado encargado) {
        this.cliente = cliente;
        this.encargado = encargado;
        this.productos = new LinkedList<>();
    }

    public PedidoClientes(Cliente cliente, Encargado encargado, int codigo_pedido, LocalDate fechaCreacion, LocalDate fechaDespacho, LocalDate fecahLlegada) throws Exception {
        this.cliente = cliente;
        this.encargado = encargado;
        this.productos = new LinkedList<>();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Encargado getEncargado() {
        return encargado;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.cliente);
        hash = 41 * hash + Objects.hashCode(this.encargado);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PedidoClientes other = (PedidoClientes) obj;
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.encargado, other.encargado)) {
            return false;
        }
        return true;
    }

   

    public Long getPk() {
        return pk;
    }

    public void setPk(Long pk) {
        this.pk = pk;
    }

  

    public void añadirProducto(Producto p){
        productos.add(p);
    }

   
    
    
    
}
