/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Clases.DetalleProducto;
import Clases.Encargado;
import Clases.PedidoProveedores;
import Clases.Producto;
import static Clases.Producto_.nombre;
import Clases.Proveedor;
import Controladores.ComprasProveedoresControl;
import DAOS.exceptions.NonexistentEntityException;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import static java.awt.image.ImageObserver.WIDTH;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Table;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *
 * @author sebastian
 */
public class ComprasProveedoresUI extends javax.swing.JInternalFrame {

    ComprasProveedoresControl ControlCompras;
    LinkedList<Producto> productosMostrar = null;
    LinkedList<Proveedor> proveedoresMostrar = null;
    Producto productoSelect;
    PedidoProveedores pedidoNuevo;
    Encargado encargadito;

    /**
     * Creates new form ComprasProveedoresUI
     */
    public ComprasProveedoresUI(ComprasProveedoresControl control) {
        this.ControlCompras = control;
        initComponents();
        ((javax.swing.plaf.basic.BasicInternalFrameUI) this.getUI()).setNorthPane(null);

        //// CREAR EL PEDIDO
        pedidoNuevo = new PedidoProveedores();

        manejadorTabla manejadorTabla = new manejadorTabla();
        tabla.setModel(manejadorTabla);
        tabla.updateUI();

        manejadorCombo comboManejador = new manejadorCombo();
        comboProveedor.setModel(comboManejador);

        /// DETECTAR CUANDO SE SELECCIONA UN OBJETO DE LA TABLA
        tabla.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                //Obtener el número de filas seleccionadas 
                int cuentaFilasSeleccionadas = tabla.getSelectedRowCount();
                System.out.println("Hay seleccionadas: " + cuentaFilasSeleccionadas + " filas");
                comboProveedor.setModel(comboManejador);
                if (cuentaFilasSeleccionadas == 1) {
                    //Sólo hay una fila seleccionada 
                    otro = 0;
                    int indiceFilaSeleccionada = tabla.getSelectedRow();
                    System.out.println("Fila seleccionada: " + indiceFilaSeleccionada);
                    productoSelect = productosMostrar.get(indiceFilaSeleccionada);
                    proveedoresMostrar = ControlCompras.buscarProveedoresCodigoProducto(productoSelect.getCodigo_producto());
                    jtextCodigo.setText(productoSelect.getCodigo_producto() + "");
                    jtextnombreProducto.setText(productoSelect.getNombre());
                    if(proveedoresMostrar.isEmpty() || proveedoresMostrar == null){
                        
                    }else{
                        comboProveedor.setSelectedIndex(0);
                        comboProveedor.updateUI();
                        comboProveedor.setSelectedIndex(0);
                        jtextCosto.setText(productosMostrar.get(0).getCostoUnidad()+"");
                    }
                    
                    /// buscar el costo del producto
                    jtextCodigo.setEditable(false);
                    jtextnombreProducto.setEditable(false);
                    jtextCantidad.setEditable(true);
                    
                    

                } else {
                    //Hay varias filas seleccionadas 
                    int[] indicesfilasSeleccionadas = tabla.getSelectedRows();
                    System.out.println("Filas seleccionadas: ");
                    for (int indice : indicesfilasSeleccionadas) {
                        System.out.print(indice + " ");
                    }
                    System.out.println();
                }
            }

        });

        comboProveedor.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
//                System.out.println(" ################ mouseClicked");
//                int selectedIndex = comboProveedor.getSelectedIndex();
//                try {
//                    jtextCosto.setText(proveedoresMostrar.get(selectedIndex).buscarProducto(productoSelect.getCodigo_producto()) + "");
//                } catch (Exception ex) {
//                    Logger.getLogger(ComprasProveedoresUI.class.getName()).log(Level.SEVERE, null, ex);
//                    JOptionPane.showMessageDialog(rootPane, ex.getMessage());
//                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println(" ################ mouseClicked");
                int selectedIndex = comboProveedor.getSelectedIndex();
                try {
                    if(productosMostrar != null || productosMostrar.isEmpty() == false){
                        jtextCosto.setText(productoSelect.getCostoUnidad() + "");
                    }
                    
                } catch (Exception ex) {
                    Logger.getLogger(ComprasProveedoresUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                System.out.println(" ################ mouseEntered");
                int selectedIndex = comboProveedor.getSelectedIndex();
                try {
                    if(productosMostrar != null || productosMostrar.isEmpty() == false){
                        jtextCosto.setText(productoSelect.getCostoUnidad() + "");
                    }
                    
                } catch (Exception ex) {
                    Logger.getLogger(ComprasProveedoresUI.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            @Override
            public void mouseExited(MouseEvent e) {
                int selectedIndex = comboProveedor.getSelectedIndex();
                try {
                    if(productosMostrar != null || productosMostrar.isEmpty() == false){
                        jtextCosto.setText(productoSelect.getCostoUnidad() + "");
                    }
                    
                } catch (Exception ex) {
                    Logger.getLogger(ComprasProveedoresUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        
        /// DETECTAR CUANDO SE LE DA ENTER AL CFAMPO MINIMO
        jtextMinimo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
            }

            @Override
            public void keyPressed(KeyEvent ke) {
                if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                    if(!productosMostrar.isEmpty()){
                        productosMostrar = control.buscarProductosPorAgotarse(Integer.parseInt(jtextMinimo.getText()));
                        tabla.updateUI();
                        comboProveedor.updateUI();
                    }
                    
                }
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });

        manejadorBotonAgregarOtro mnj = new manejadorBotonAgregarOtro();
        BtnAñadir.addActionListener(mnj);
        
        enter ent = new enter();
        jtextCantidad.addKeyListener(ent);
        
        manejadorBotonPedido manejadorGuardar = new manejadorBotonPedido();
        BtnSolicitar.addActionListener(manejadorGuardar);
        
        manejadorBuscarProducto botonBuscar = new manejadorBuscarProducto();
        BtnBuscar.addActionListener(botonBuscar);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabla = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        BtnAñadir = new interfaces.MiButton();
        jtextMinimo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jtextCodigo = new javax.swing.JTextField();
        BtnBuscar = new interfaces.MiButton();
        jLabel5 = new javax.swing.JLabel();
        jtextnombreProducto = new javax.swing.JTextField();
        comboProveedor = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jtextCosto = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jlabelCosto = new javax.swing.JLabel();
        jtextCantidad = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jlabelCostoTotal = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        BtnSolicitar = new interfaces.MiButton();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jLabel6.setText("jLabel6");

        setBorder(null);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tabla);

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel1.setText("Productos por agotarse:");

        BtnAñadir.setText("Añadir Otro");
        BtnAñadir.setColorHover(new java.awt.Color(0, 204, 204));
        BtnAñadir.setColorNormal(new java.awt.Color(0, 102, 102));

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel8.setText("Cantidad minima:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(BtnAñadir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(159, 159, 159))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jtextMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(33, 33, 33))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jtextMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(BtnAñadir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel3.setText("codigo del producto:");

        jLabel4.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel4.setText("nombre del producto:");

        jtextCodigo.setEditable(false);
        jtextCodigo.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        BtnBuscar.setText("...");
        BtnBuscar.setColorHover(new java.awt.Color(0, 204, 204));
        BtnBuscar.setColorNormal(new java.awt.Color(0, 102, 102));

        jLabel5.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel5.setText("proveedor:");

        jtextnombreProducto.setEditable(false);
        jtextnombreProducto.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        comboProveedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboProveedor.setPreferredSize(new java.awt.Dimension(56, 26));

        jLabel7.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel7.setText("costo:");

        jtextCosto.setEditable(false);
        jtextCosto.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel9.setText("costo total:");

        jtextCantidad.setEditable(false);
        jtextCantidad.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel10.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jLabel10.setText("cantidad:");

        jlabelCostoTotal.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jlabelCostoTotal.setText("...");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(18, 18, 18))
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel7))
                            .addGap(26, 26, 26)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel10))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jtextCodigo)
                    .addComponent(jtextnombreProducto)
                    .addComponent(comboProveedor, 0, 152, Short.MAX_VALUE)
                    .addComponent(jtextCosto)
                    .addComponent(jtextCantidad))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jlabelCosto, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jlabelCostoTotal)))
                    .addComponent(BtnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jtextCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(BtnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtextnombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlabelCosto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jtextCosto, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)
                            .addComponent(jlabelCostoTotal))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jtextCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jPanel3.setBackground(new java.awt.Color(197, 13, 73));

        jLabel2.setFont(new java.awt.Font("Verdana", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Informacion del producto:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        BtnSolicitar.setText("Solicitar pedido");
        BtnSolicitar.setColorHover(new java.awt.Color(0, 204, 204));
        BtnSolicitar.setColorNormal(new java.awt.Color(0, 102, 102));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(146, 146, 146)
                        .addComponent(BtnSolicitar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(BtnSolicitar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private interfaces.MiButton BtnAñadir;
    private interfaces.MiButton BtnBuscar;
    private interfaces.MiButton BtnSolicitar;
    private javax.swing.JComboBox<String> comboProveedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel jlabelCosto;
    private javax.swing.JLabel jlabelCostoTotal;
    private javax.swing.JTextField jtextCantidad;
    private javax.swing.JTextField jtextCodigo;
    private javax.swing.JTextField jtextCosto;
    private javax.swing.JTextField jtextMinimo;
    private javax.swing.JTextField jtextnombreProducto;
    private javax.swing.JTable tabla;
    // End of variables declaration//GEN-END:variables

    public class manejadorTabla implements TableModel {

        String nombres[] = {"codigo_producto", "codigo_proveedores", "nombre", "cantidad"};

        @Override
        public int getRowCount() {
            if (productosMostrar == null) {
                return 0;
            }
            return productosMostrar.size();
        }

        @Override
        public int getColumnCount() {
            return nombres.length;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return nombres[0];
                case 1:
                    return nombres[1];
                case 2:
                    return nombres[2];
                case 3:
                    return nombres[3];
            }
            return "";
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            //permisos = sedeLocal.buscarPermisosRecientes();
            
            if (productosMostrar == null) {
                return 0;
            }
            Producto pdto = productosMostrar.get(rowIndex);

            switch (columnIndex) {
                case 0:
                    return pdto.getCodigo_producto();
                case 1:
                    return pdto.getProveedor().getCodigoProveedor();
                case 2:
                    return pdto.getNombre();
                case 3:
                    return pdto.getCantidad();
            }
            return "";
        }

        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        }

        @Override
        public void addTableModelListener(TableModelListener l) {

        }

        @Override
        public void removeTableModelListener(TableModelListener l) {
        }

    }

    String proveedorSeleccionado = null;

    public class manejadorCombo implements ComboBoxModel {

        @Override
        public void setSelectedItem(Object o) {
            proveedorSeleccionado = (String) o;
        }

        @Override
        public Object getSelectedItem() {
            if (proveedoresMostrar == null) {
                return "";
            }
            return proveedorSeleccionado;
        }

        @Override
        public int getSize() {
            if (proveedoresMostrar == null) {
                return 1;
            }
            return proveedoresMostrar.size();
        }

        @Override
        public Object getElementAt(int i) {

            if (tabla.getSelectedRow() == -1) {
                return "";
            }
            int producto = productosMostrar.get(tabla.getSelectedRow()).getCodigo_producto();
            return proveedoresMostrar.get(i).getNombre() + "";
        }

        @Override
        public void addListDataListener(ListDataListener ll) {
        }

        @Override
        public void removeListDataListener(ListDataListener ll) {
        }

    }
int otro = 0;
    public class manejadorBotonAgregarOtro implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            jtextCodigo.setText("");
            jtextnombreProducto.setText("");
            jtextCodigo.setEditable(true);
            jtextCantidad.setEditable(true);
            otro = 1;
            
        }

    }
    // Producto productoOther;
    public class manejadorBuscarProducto implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
            int codigo = Integer.parseInt(jtextCodigo.getText());
            try {
                productoSelect= ControlCompras.buscarProducto(codigo);
                jtextnombreProducto.setText(productoSelect.getNombre());
                jtextCosto.setText(productoSelect.getCostoUnidad() + "");
                // modificar el combobox`para que solo muestre el proveedor del producto
                
                comboProveedor.setModel(new ComboBoxModel<String>() {
                    String proveedora = "";
                    @Override
                    public void setSelectedItem(Object o) {
                        proveedora = (String) o;
                    }

                    @Override
                    public Object getSelectedItem() {
                        return proveedora;
                    }

                    @Override
                    public int getSize() {
                        return 1;
                    }

                    @Override
                    public String getElementAt(int i) {
                        return productoSelect.getNombre();
                    }

                    @Override
                    public void addListDataListener(ListDataListener ll) {
                    }

                    @Override
                    public void removeListDataListener(ListDataListener ll) {
                    }
                });
                
            } catch (Exception ex) {
                Logger.getLogger(ComprasProveedoresUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
    
    public class enter implements KeyListener{

        @Override
        public void keyTyped(KeyEvent ke) {
        }

        @Override
        public void keyPressed(KeyEvent ke) {
            if(ke.getKeyCode() == KeyEvent.VK_ENTER){
                            
                            jlabelCostoTotal.setText(productoSelect.getCostoUnidad() * Integer.parseInt(jtextCantidad.getText()) + "");
                        
            }
        }

        @Override
        public void keyReleased(KeyEvent ke) {
        }

        
        
    }
    
    
    public class manejadorBotonPedido implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent ae) {
            if(jtextCodigo.equals("")){
                JOptionPane.showMessageDialog(rootPane, "ingrese el codigo de producto por favor");
            }else{
                if(otro == 1){
                pedidoNuevo.setEncargado(encargadito);
                pedidoNuevo.setProveedor(productoSelect.getProveedor());
                
            }else{
            pedidoNuevo.setEncargado(encargadito);
            pedidoNuevo.setProveedor(proveedoresMostrar.get(comboProveedor.getSelectedIndex()));
            System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" + productoSelect.getNombre());
                
            }
            pedidoNuevo.añadirProducto(productoSelect);
            pedidoNuevo.setEstado("Generado");
            pedidoNuevo.setFechaCreacion(LocalDate.now());
            
            ControlCompras.guardarPedido(pedidoNuevo);
            JOptionPane.showMessageDialog(rootPane, "Pedido registrado con exito");
            
            jtextCodigo.setText("");
            jtextCantidad.setText("");
            jtextnombreProducto.setText("");
            jtextCodigo.setEditable(false);
            jtextCantidad.setEditable(false);
            }
            
            
        }
        
    }
}
