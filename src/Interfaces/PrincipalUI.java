/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Controladores.ComprasProveedoresControl;
import Controladores.ControlPedidoCliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;

/**
 *
 * @author sebastian
 */
public class PrincipalUI extends javax.swing.JFrame {
    
    ComprasProveedoresControl controlProveedores;
    ControlPedidoCliente controlPedidos;
    
    /**
     * Creates new form Principal
     */
    public PrincipalUI(ComprasProveedoresControl ctrl , ControlPedidoCliente ctrl2) {
        this.controlProveedores = ctrl;
        this.controlPedidos = ctrl2;
        initComponents();
        ManejadorGestionarProductos manejadorGestionar = new ManejadorGestionarProductos();
        ManejadorComprasProveedor manejadorProveedores = new ManejadorComprasProveedor();
        ManejadorPedidoCliente manejadorPedido = new ManejadorPedidoCliente();
        BtncomprasProveedores.addActionListener(manejadorProveedores);
        BtnGestionarProducto.addActionListener(manejadorGestionar);
        BtnPedidoCliente.addActionListener(manejadorPedido);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        BtnGestionarProducto = new interfaces.MiButton();
        BtncomprasProveedores = new interfaces.MiButton();
        jLabel2 = new javax.swing.JLabel();
        BtnPedidoCliente = new interfaces.MiButton();
        desktop = new javax.swing.JDesktopPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(0, 149, 147));

        BtnGestionarProducto.setBackground(new java.awt.Color(0, 127, 127));
        BtnGestionarProducto.setText("gestionar producto");
        BtnGestionarProducto.setColorHover(new java.awt.Color(0, 200, 200));
        BtnGestionarProducto.setColorNormal(new java.awt.Color(0, 127, 127));
        BtnGestionarProducto.setColorPressed(new java.awt.Color(0, 127, 127));
        BtnGestionarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnGestionarProductoActionPerformed(evt);
            }
        });

        BtncomprasProveedores.setBackground(new java.awt.Color(0, 127, 127));
        BtncomprasProveedores.setText("compras Proveedores");
        BtncomprasProveedores.setColorHover(new java.awt.Color(0, 200, 200));
        BtncomprasProveedores.setColorNormal(new java.awt.Color(0, 127, 127));
        BtncomprasProveedores.setColorPressed(new java.awt.Color(0, 127, 127));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/open-menu.png"))); // NOI18N

        BtnPedidoCliente.setBackground(new java.awt.Color(0, 127, 127));
        BtnPedidoCliente.setText("pedidos cliente");
        BtnPedidoCliente.setColorHover(new java.awt.Color(0, 200, 200));
        BtnPedidoCliente.setColorNormal(new java.awt.Color(0, 127, 127));
        BtnPedidoCliente.setColorPressed(new java.awt.Color(0, 127, 127));
        BtnPedidoCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnPedidoClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(BtnGestionarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtncomprasProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BtnPedidoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 10, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(BtncomprasProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnGestionarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(BtnPedidoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 377, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        desktop.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 495, Short.MAX_VALUE)
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(desktop)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(desktop)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void BtnGestionarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnGestionarProductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnGestionarProductoActionPerformed

    private void BtnPedidoClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnPedidoClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BtnPedidoClienteActionPerformed

    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private interfaces.MiButton BtnGestionarProducto;
    private interfaces.MiButton BtnPedidoCliente;
    private interfaces.MiButton BtncomprasProveedores;
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel2;
    // End of variables declaration//GEN-END:variables
ComprasProveedoresUI comprasProveedor = null;
    public class ManejadorComprasProveedor implements ActionListener {
       
       
        @Override
        public void actionPerformed(ActionEvent arg0) {
            if(comprasProveedor == null){   // si ya está creado
                comprasProveedor = new ComprasProveedoresUI(controlProveedores); // creelo
            }
            if(ventanaProducto != null){
             ventanaProducto.setVisible(false);
             desktop.remove(ventanaProducto);
            }
            comprasProveedor.setVisible(true);
            desktop.add(comprasProveedor);  // y añadalo al desktop
            
        }
        
    }
gestionarProductosUI ventanaProducto;
public class ManejadorGestionarProductos implements ActionListener{
        
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            if(ventanaProducto == null){
                ventanaProducto = new gestionarProductosUI();
            }
            if(comprasProveedor != null){
                comprasProveedor.setVisible(false);
                desktop.remove(comprasProveedor);
            }
            ventanaProducto.setVisible(true);
            desktop.add(ventanaProducto);
            
        }
    
}
PedidoClienteUI ventanaPedido;
public class ManejadorPedidoCliente implements ActionListener{
        
        
        @Override
        public void actionPerformed(ActionEvent ae) {
            if(ventanaPedido == null){
                ventanaPedido = new PedidoClienteUI(controlPedidos);
            }
            
            if(comprasProveedor != null){
                comprasProveedor.setVisible(false);
                desktop.remove(comprasProveedor);
            }
            if(ventanaProducto != null){
             ventanaProducto.setVisible(false);
             desktop.remove(ventanaProducto);
            }
            ventanaPedido.setVisible(true);
            desktop.add(ventanaPedido);
            
        }
    
}
}
