/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Clases.Producto;
import Clases.Proveedor;
import DAOS.exceptions.NonexistentEntityException;
import DAOS.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author juan
 */
public class ProveedorJpaController implements Serializable {

    public ProveedorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Proveedor proveedor) throws PreexistingEntityException, Exception {
        if (proveedor.getListaProductos() == null) {
            proveedor.setListaproductos(new ArrayList<Producto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Producto> attachedListaproductos = new ArrayList<Producto>();
            for (Producto listaproductosProductoToAttach : proveedor.getListaProductos()) {
                listaproductosProductoToAttach = em.getReference(listaproductosProductoToAttach.getClass(), listaproductosProductoToAttach.getCodigo_producto());
                attachedListaproductos.add(listaproductosProductoToAttach);
            }
            proveedor.setListaproductos(attachedListaproductos);
            em.persist(proveedor);
            for (Producto listaproductosProducto : proveedor.getListaProductos()) {
                Proveedor oldProveedorOfListaproductosProducto = listaproductosProducto.getProveedor();
                listaproductosProducto.setProveedor(proveedor);
                listaproductosProducto = em.merge(listaproductosProducto);
                if (oldProveedorOfListaproductosProducto != null) {
                    oldProveedorOfListaproductosProducto.getListaProductos().remove(listaproductosProducto);
                    oldProveedorOfListaproductosProducto = em.merge(oldProveedorOfListaproductosProducto);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProveedor(proveedor.getCodigoProveedor()) != null) {
                throw new PreexistingEntityException("Proveedor " + proveedor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Proveedor proveedor) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor persistentProveedor = em.find(Proveedor.class, proveedor.getCodigoProveedor());
            List<Producto> listaproductosOld = persistentProveedor.getListaProductos();
            List<Producto> listaproductosNew = proveedor.getListaProductos();
            List<Producto> attachedListaproductosNew = new ArrayList<Producto>();
            for (Producto listaproductosNewProductoToAttach : listaproductosNew) {
                listaproductosNewProductoToAttach = em.getReference(listaproductosNewProductoToAttach.getClass(), listaproductosNewProductoToAttach.getCodigo_producto());
                attachedListaproductosNew.add(listaproductosNewProductoToAttach);
            }
            listaproductosNew = attachedListaproductosNew;
            proveedor.setListaproductos(listaproductosNew);
            proveedor = em.merge(proveedor);
            for (Producto listaproductosOldProducto : listaproductosOld) {
                if (!listaproductosNew.contains(listaproductosOldProducto)) {
                    listaproductosOldProducto.setProveedor(null);
                    listaproductosOldProducto = em.merge(listaproductosOldProducto);
                }
            }
            for (Producto listaproductosNewProducto : listaproductosNew) {
                if (!listaproductosOld.contains(listaproductosNewProducto)) {
                    Proveedor oldProveedorOfListaproductosNewProducto = listaproductosNewProducto.getProveedor();
                    listaproductosNewProducto.setProveedor(proveedor);
                    listaproductosNewProducto = em.merge(listaproductosNewProducto);
                    if (oldProveedorOfListaproductosNewProducto != null && !oldProveedorOfListaproductosNewProducto.equals(proveedor)) {
                        oldProveedorOfListaproductosNewProducto.getListaProductos().remove(listaproductosNewProducto);
                        oldProveedorOfListaproductosNewProducto = em.merge(oldProveedorOfListaproductosNewProducto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = proveedor.getCodigoProveedor();
                if (findProveedor(id) == null) {
                    throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor proveedor;
            try {
                proveedor = em.getReference(Proveedor.class, id);
                proveedor.getCodigoProveedor();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The proveedor with id " + id + " no longer exists.", enfe);
            }
            List<Producto> listaproductos = proveedor.getListaProductos();
            for (Producto listaproductosProducto : listaproductos) {
                listaproductosProducto.setProveedor(null);
                listaproductosProducto = em.merge(listaproductosProducto);
            }
            em.remove(proveedor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Proveedor> findProveedorEntities() {
        return findProveedorEntities(true, -1, -1);
    }

    public List<Proveedor> findProveedorEntities(int maxResults, int firstResult) {
        return findProveedorEntities(false, maxResults, firstResult);
    }

    private List<Proveedor> findProveedorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Proveedor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Proveedor findProveedor(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Proveedor.class, id);
        } finally {
            em.close();
        }
    }

    public int getProveedorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Proveedor> rt = cq.from(Proveedor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public LinkedList<Proveedor> getProveedoresDitribucion (int codigo){
        
        List<Proveedor> lista = findProveedorEntities();
        System.out.println("cantidad de proveedores total : " + lista.size());
        LinkedList<Proveedor> casting = new LinkedList<>();
        for(Proveedor p : lista){
            casting.add(p);
        }
        System.out.println("cantidad antes del casting de proveedores : " + casting.size());
        LinkedList<Proveedor> proveedoresEncontrados = new LinkedList<>();
        
        for(Proveedor p : lista){
            if(p.ditribuyeProducto(codigo)){
                proveedoresEncontrados.add(p);
            }
            System.out.println("me repetiiiiii");
        }
        System.out.println("cantidad de proveedores con el codigo " + codigo +" " + proveedoresEncontrados.get(0).getNombre() );
        return proveedoresEncontrados;
    }
}
