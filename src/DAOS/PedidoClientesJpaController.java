/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Clases.PedidoClientes;
import DAOS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author juan
 */
public class PedidoClientesJpaController implements Serializable {

    public PedidoClientesJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PedidoClientes pedidoClientes) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(pedidoClientes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PedidoClientes pedidoClientes) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            pedidoClientes = em.merge(pedidoClientes);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = pedidoClientes.getPk();
                if (findPedidoClientes(id) == null) {
                    throw new NonexistentEntityException("The pedidoClientes with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PedidoClientes pedidoClientes;
            try {
                pedidoClientes = em.getReference(PedidoClientes.class, id);
                pedidoClientes.getPk();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedidoClientes with id " + id + " no longer exists.", enfe);
            }
            em.remove(pedidoClientes);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PedidoClientes> findPedidoClientesEntities() {
        return findPedidoClientesEntities(true, -1, -1);
    }

    public List<PedidoClientes> findPedidoClientesEntities(int maxResults, int firstResult) {
        return findPedidoClientesEntities(false, maxResults, firstResult);
    }

    private List<PedidoClientes> findPedidoClientesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PedidoClientes.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PedidoClientes findPedidoClientes(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PedidoClientes.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoClientesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PedidoClientes> rt = cq.from(PedidoClientes.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
