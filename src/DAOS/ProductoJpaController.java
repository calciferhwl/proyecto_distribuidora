/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Clases.Proveedor;
import Clases.DetalleProducto;
import Clases.Producto;
import DAOS.exceptions.NonexistentEntityException;
import DAOS.exceptions.PreexistingEntityException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

/**
 *
 * @author juan
 */
public class ProductoJpaController implements Serializable {

    public ProductoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Producto producto) throws PreexistingEntityException, Exception {
        if (producto.getDetalleProductos() == null) {
            producto.setDetalleProductos(new ArrayList<DetalleProducto>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Proveedor proveedor = producto.getProveedor();
            if (proveedor != null) {
                em.persist(proveedor);
                proveedor = em.getReference(proveedor.getClass(), proveedor.getCodigoProveedor());
                producto.setProveedor(proveedor);
            }
            List<DetalleProducto> attachedDetalleProductos = new ArrayList<DetalleProducto>();
            for (DetalleProducto detalleProductosDetalleProductoToAttach : producto.getDetalleProductos()) {
                detalleProductosDetalleProductoToAttach = em.getReference(detalleProductosDetalleProductoToAttach.getClass(), detalleProductosDetalleProductoToAttach.getId());
                attachedDetalleProductos.add(detalleProductosDetalleProductoToAttach);
            }
            producto.setDetalleProductos(attachedDetalleProductos);
            em.persist(producto);
            
            for (DetalleProducto detalleProductosDetalleProducto : producto.getDetalleProductos()) {
                Producto oldProductoOfDetalleProductosDetalleProducto = detalleProductosDetalleProducto.getProducto();
                detalleProductosDetalleProducto.setProducto(producto);
                detalleProductosDetalleProducto = em.merge(detalleProductosDetalleProducto);
                if (oldProductoOfDetalleProductosDetalleProducto != null) {
                    oldProductoOfDetalleProductosDetalleProducto.getDetalleProductos().remove(detalleProductosDetalleProducto);
                    oldProductoOfDetalleProductosDetalleProducto = em.merge(oldProductoOfDetalleProductosDetalleProducto);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findProducto(producto.getCodigo_producto()) != null) {
                throw new PreexistingEntityException("Producto " + producto + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Producto producto) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto persistentProducto = em.find(Producto.class, producto.getCodigo_producto());
            Proveedor proveedorOld = persistentProducto.getProveedor();
            Proveedor proveedorNew = producto.getProveedor();
            List<DetalleProducto> detalleProductosOld = persistentProducto.getDetalleProductos();
            List<DetalleProducto> detalleProductosNew = producto.getDetalleProductos();
            if (proveedorNew != null) {
                proveedorNew = em.getReference(proveedorNew.getClass(), proveedorNew.getCodigoProveedor());
                producto.setProveedor(proveedorNew);
            }
            List<DetalleProducto> attachedDetalleProductosNew = new ArrayList<DetalleProducto>();
            for (DetalleProducto detalleProductosNewDetalleProductoToAttach : detalleProductosNew) {
                detalleProductosNewDetalleProductoToAttach = em.getReference(detalleProductosNewDetalleProductoToAttach.getClass(), detalleProductosNewDetalleProductoToAttach.getId());
                attachedDetalleProductosNew.add(detalleProductosNewDetalleProductoToAttach);
            }
            detalleProductosNew = attachedDetalleProductosNew;
            producto.setDetalleProductos( detalleProductosNew); ///////$%&"$%
            producto = em.merge(producto);
            
            for (DetalleProducto detalleProductosOldDetalleProducto : detalleProductosOld) {
                if (!detalleProductosNew.contains(detalleProductosOldDetalleProducto)) {
                    detalleProductosOldDetalleProducto.setProducto(null);
                    detalleProductosOldDetalleProducto = em.merge(detalleProductosOldDetalleProducto);
                }
            }
            for (DetalleProducto detalleProductosNewDetalleProducto : detalleProductosNew) {
                if (!detalleProductosOld.contains(detalleProductosNewDetalleProducto)) {
                    Producto oldProductoOfDetalleProductosNewDetalleProducto = detalleProductosNewDetalleProducto.getProducto();
                    detalleProductosNewDetalleProducto.setProducto(producto);
                    detalleProductosNewDetalleProducto = em.merge(detalleProductosNewDetalleProducto);
                    if (oldProductoOfDetalleProductosNewDetalleProducto != null && !oldProductoOfDetalleProductosNewDetalleProducto.equals(producto)) {
                        oldProductoOfDetalleProductosNewDetalleProducto.getDetalleProductos().remove(detalleProductosNewDetalleProducto);
                        oldProductoOfDetalleProductosNewDetalleProducto = em.merge(oldProductoOfDetalleProductosNewDetalleProducto);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = producto.getCodigo_producto();
                if (findProducto(id) == null) {
                    throw new NonexistentEntityException("The producto with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Producto producto;
            try {
                producto = em.getReference(Producto.class, id);
                producto.getCodigo_producto();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The producto with id " + id + " no longer exists.", enfe);
            }
            
            List<DetalleProducto> detalleProductos = producto.getDetalleProductos();
            for (DetalleProducto detalleProductosDetalleProducto : detalleProductos) {
                detalleProductosDetalleProducto.setProducto(null);
                detalleProductosDetalleProducto = em.merge(detalleProductosDetalleProducto);
            }
            em.remove(producto);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Producto> findProductoEntities() {
        return findProductoEntities(true, -1, -1);
    }

    public List<Producto> findProductoEntities(int maxResults, int firstResult) {
        return findProductoEntities(false, maxResults, firstResult);
    }

    private List<Producto> findProductoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Producto.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Producto findProducto(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Producto.class, id);
        } finally {
            em.close();
        }
    }

    public int getProductoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Producto> rt = cq.from(Producto.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    //// buscar productos que estén por agotarse
    public LinkedList<Producto> getProductosAgotarse(int cantidadMinima) {
        EntityManager em = getEntityManager();
        Query query = em.createQuery("SELECT p FROM Producto p "
                + "WHERE p.cantidad < :cantidadMinima");
        query.setParameter("cantidadMinima", cantidadMinima);
        List<Producto> lista = query.getResultList();
        System.out.println("productos por agotarse : " + lista.size());
        LinkedList<Producto> productosEncontrados = new LinkedList<>();
        for(Producto p : lista){
            productosEncontrados.add(p);
        }
        
        return productosEncontrados;
    }

    public Producto buscarProducto(int codigo_producto) throws Exception {
        EntityManager em = getEntityManager();
        try {
            
            Producto p = (Producto) em.createNamedQuery("buscarPorCodigo")
                    .setParameter("codigo", codigo_producto)
                    .getSingleResult();
            
            return p;
                    
                    
        }catch(NoResultException ex){
            throw new Exception ("no se encontro el producto");
        }finally {
            em.close();
        }
    }
}
