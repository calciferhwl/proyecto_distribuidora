/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Clases.DetallePedidoCliente;
import DAOS.exceptions.NonexistentEntityException;
import DAOS.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author juan
 */
public class DetallePedidoClienteJpaController implements Serializable {

    public DetallePedidoClienteJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetallePedidoCliente detallePedidoCliente) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(detallePedidoCliente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetallePedidoCliente(detallePedidoCliente.getCodigo_pedido()) != null) {
                throw new PreexistingEntityException("DetallePedidoCliente " + detallePedidoCliente + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetallePedidoCliente detallePedidoCliente) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            detallePedidoCliente = em.merge(detallePedidoCliente);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = detallePedidoCliente.getCodigo_pedido();
                if (findDetallePedidoCliente(id) == null) {
                    throw new NonexistentEntityException("The detallePedidoCliente with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetallePedidoCliente detallePedidoCliente;
            try {
                detallePedidoCliente = em.getReference(DetallePedidoCliente.class, id);
                detallePedidoCliente.getCodigo_pedido();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detallePedidoCliente with id " + id + " no longer exists.", enfe);
            }
            em.remove(detallePedidoCliente);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetallePedidoCliente> findDetallePedidoClienteEntities() {
        return findDetallePedidoClienteEntities(true, -1, -1);
    }

    public List<DetallePedidoCliente> findDetallePedidoClienteEntities(int maxResults, int firstResult) {
        return findDetallePedidoClienteEntities(false, maxResults, firstResult);
    }

    private List<DetallePedidoCliente> findDetallePedidoClienteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetallePedidoCliente.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetallePedidoCliente findDetallePedidoCliente(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetallePedidoCliente.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetallePedidoClienteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetallePedidoCliente> rt = cq.from(DetallePedidoCliente.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
