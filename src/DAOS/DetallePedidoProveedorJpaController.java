/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Clases.DetallePedidoProveedor;
import DAOS.exceptions.NonexistentEntityException;
import DAOS.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author juan
 */
public class DetallePedidoProveedorJpaController implements Serializable {

    public DetallePedidoProveedorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DetallePedidoProveedor detallePedidoProveedor) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(detallePedidoProveedor);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findDetallePedidoProveedor(detallePedidoProveedor.getCodigo_pedido()) != null) {
                throw new PreexistingEntityException("DetallePedidoProveedor " + detallePedidoProveedor + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DetallePedidoProveedor detallePedidoProveedor) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            detallePedidoProveedor = em.merge(detallePedidoProveedor);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = detallePedidoProveedor.getCodigo_pedido();
                if (findDetallePedidoProveedor(id) == null) {
                    throw new NonexistentEntityException("The detallePedidoProveedor with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(int id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DetallePedidoProveedor detallePedidoProveedor;
            try {
                detallePedidoProveedor = em.getReference(DetallePedidoProveedor.class, id);
                detallePedidoProveedor.getCodigo_pedido();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The detallePedidoProveedor with id " + id + " no longer exists.", enfe);
            }
            em.remove(detallePedidoProveedor);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DetallePedidoProveedor> findDetallePedidoProveedorEntities() {
        return findDetallePedidoProveedorEntities(true, -1, -1);
    }

    public List<DetallePedidoProveedor> findDetallePedidoProveedorEntities(int maxResults, int firstResult) {
        return findDetallePedidoProveedorEntities(false, maxResults, firstResult);
    }

    private List<DetallePedidoProveedor> findDetallePedidoProveedorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DetallePedidoProveedor.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DetallePedidoProveedor findDetallePedidoProveedor(int id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DetallePedidoProveedor.class, id);
        } finally {
            em.close();
        }
    }

    public int getDetallePedidoProveedorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DetallePedidoProveedor> rt = cq.from(DetallePedidoProveedor.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
