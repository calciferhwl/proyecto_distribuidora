/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOS;

import Clases.PedidoProveedores;
import Clases.PedidoProveedores_;
import Clases.Producto;
import Clases.Proveedor;
import DAOS.exceptions.NonexistentEntityException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author sebastian
 */
public class PedidoProveedoresJpaController implements Serializable {

    public PedidoProveedoresJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PedidoProveedores pedidoProveedores) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(pedidoProveedores);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PedidoProveedores pedidoProveedores) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            pedidoProveedores = em.merge(pedidoProveedores);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                long id = pedidoProveedores.getPk();
                if (findPedidoProveedores(id) == null) {
                    throw new NonexistentEntityException("The pedidoProveedores with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PedidoProveedores pedidoProveedores;
            try {
                pedidoProveedores = em.getReference(PedidoProveedores.class, id);
                pedidoProveedores.getPk();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pedidoProveedores with id " + id + " no longer exists.", enfe);
            }
            em.remove(pedidoProveedores);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PedidoProveedores> findPedidoProveedoresEntities() {
        return findPedidoProveedoresEntities(true, -1, -1);
    }

    public List<PedidoProveedores> findPedidoProveedoresEntities(int maxResults, int firstResult) {
        return findPedidoProveedoresEntities(false, maxResults, firstResult);
    }

    private List<PedidoProveedores> findPedidoProveedoresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PedidoProveedores.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PedidoProveedores findPedidoProveedores(long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PedidoProveedores.class, id);
        } finally {
            em.close();
        }
    }

    public int getPedidoProveedoresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PedidoProveedores> rt = cq.from(PedidoProveedores.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public PedidoProveedores buscarPedidoPorProveedor(int codigo_proveedor) {
        EntityManager em = getEntityManager();
        try {
            return (PedidoProveedores) em.createNamedQuery("buscarPorProveedor")
                    .setParameter("codigo", codigo_proveedor)
                    .getSingleResult();
        } finally {
            em.close();
        }
    }
}
